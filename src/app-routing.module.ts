import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: '', loadChildren: './components/server/server.module#ServerModule' }
];

export const AppRoutes = RouterModule.forRoot(routes, { useHash: false });

