export interface Server {
    model: string;
    ram: object;
    hdd: object;
    location: string;
    price: object;
}
