import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {ServerModule} from './components/server/server.module';

import { AppRoutes } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {NotifierModule} from 'angular-notifier';
import {Ng5SliderModule} from 'ng5-slider';


@NgModule({
  imports:      [ BrowserModule, HttpClientModule , AppRoutes, ServerModule, NotifierModule, Ng5SliderModule ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
