import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SharedService {

    constructor() {
    }

    private subject = new Subject<any>();

    updateFilter(filter: object) {
        this.subject.next({filter});
    }


    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

    setCities(cities: any) {
        this.subject.next({cities});
    }


    setHddTypes(hddTypes: any) {
        this.subject.next({hddTypes});
    }

}
