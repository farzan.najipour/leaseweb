import {
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpSentEvent,
    HttpHeaderResponse,
    HttpProgressEvent,
    HttpResponse,
    HttpUserEvent
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {API_CONSTANTS} from '../constants';

const basicUrls = ['ticket', 'token'];

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {

    constructor() {
    }

    headersConfig: any;

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {
        return next.handle(this.addTokenToRequest(req)).pipe(
            catchError(error => {
                return throwError(error);
            }),
        );
    }

    private addTokenToRequest(request: HttpRequest<any>): HttpRequest<any> {
        this.headersConfig = {
            Agent: API_CONSTANTS.HTTP_AGENT,
            'Content-Type': 'application/json'
        };
        return request.clone({setHeaders: this.headersConfig});
    }
}






