import {TestBed} from '@angular/core/testing';

import {ApiService} from './api.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../../environments/environment';

describe('ApiService', () => {
    let httpMock: HttpTestingController;
    let apiService: ApiService;

    beforeEach(() => {

        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [ApiService]
        });

        apiService = TestBed.get(ApiService);
        httpMock = TestBed.get(HttpTestingController);

    });

    afterEach(() => {
        httpMock.verify();
    });

    it('getData should http GET servers', () => {
        apiService.get('api/servers').subscribe((res) => {
            expect(res).toBeDefined();
        });

        const mockReq = httpMock.expectOne(`${environment.api_url}api/servers`);

        expect(mockReq.request.method).toBe('GET');

    });
});
