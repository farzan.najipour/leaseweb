import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PreventProductionGuardService {

  constructor() {
  }

  canActivate(): boolean {
    return environment.name !== 'production';
  }
}
