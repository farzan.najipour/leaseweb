import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup, FormControl, FormBuilder, FormArray} from '@angular/forms';
import {SharedService} from '../../../services/shared.service';
import {Subscription} from 'rxjs';
import {LabelType, Options} from 'ng5-slider';
import {HDD_STORAGE, RAM} from '../enum';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
    form: FormGroup;
    cities = [];
    hddTypes = [];
    finalMinValue = HDD_STORAGE.min;
    finalMaxValue = HDD_STORAGE.max;
    minValue = HDD_STORAGE.min;
    maxValue = HDD_STORAGE.max;
    options: Options = {
        floor: HDD_STORAGE.min,
        ceil: HDD_STORAGE.max,
        translate: (value: number, label: LabelType): string => {
            switch (label) {
                case LabelType.Low:
                    return `<b>Min:</b> ${value} GB`;
                case LabelType.High:
                    return `<b>Max:</b> ${value} GB`;
                default:
                    return `${value} GB`;
            }
        },
        step: 1000,
        showTicks: true
    };
    ramConfigs = RAM;
    subscription: Subscription;

    constructor(private fb: FormBuilder, private sharedService: SharedService) {
        this.subscription = this.sharedService.getMessage().subscribe(message => {
            if (message.cities) {
                this.cities = message.cities;
            }
            if (message.hddTypes) {
                this.hddTypes = message.hddTypes;
            }
        });
    }

    ngOnInit(): void {
        this.buildForm();
    }

    checkBoxChanged(event) {
        const formArray: FormArray = this.form.get('ram') as FormArray;

        /* Selected */
        if (event.target.checked) {
            // Add a new control in the arrayForm
            formArray.push(new FormControl(event.target.value));
        } else {
            // find the unselected element
            let i = 0;

            formArray.controls.forEach((ctrl: FormControl) => {
                if (ctrl.value === event.target.value) {
                    // Remove the unselected element from the arrayForm
                    formArray.removeAt(i);
                    return;
                }
                i++;
            });
        }
    }


    buildForm(): void {
        this.form = this.fb.group({
            location: new FormControl(''),
            type: new FormControl(''),
            ram: new FormArray([]),
        });
    }

    /**
     * Changing value of ng5-slider's maximum value
     * @param event
     */
    highValueChanged(event) {
        this.finalMaxValue = event;
    }

    /**
     * Changing value of ng5-slider's minimum value
     * @param event
     */
    valueChanged(event) {
        this.finalMinValue = event;
    }

    /**
     * When user clicked on button , this method fire, it creates new filter and pass it to sharedService
     * @param filters
     */
    search(filters: any): void {
        Object.keys(filters).forEach(key => filters[key] === '' ? delete filters[key] : key);
        filters.storage = {
            min: this.finalMinValue,
            max: this.finalMaxValue
        };
        this.sharedService.updateFilter(filters);
    }

    /**
     * Unsubscribe to ensure no memory leaks
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
