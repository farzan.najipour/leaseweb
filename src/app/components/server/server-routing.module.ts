import { RouterModule, Routes } from '@angular/router';
import { ServerComponent } from './server.component';

const routes: Routes = [
  { path: '', component: ServerComponent }
];

export const ServerRoutes = RouterModule.forChild(routes);

