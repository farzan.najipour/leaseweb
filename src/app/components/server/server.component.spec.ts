import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {ServerComponent} from './server.component';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {SearchComponent} from './search/search.component';
import {ServerListComponent} from './server-list/server-list.component';
import {MockComponent} from 'ng-mocks';
import {HeaderComponent} from './header/header.component';

describe('ServerComponent', () => {
    let component: ServerComponent;
    let fixture: ComponentFixture<ServerComponent>;
    let de: DebugElement;
    beforeEach(() => {
        fixture = TestBed.configureTestingModule({
            declarations: [ ServerComponent,
                MockComponent(SearchComponent),
                MockComponent(HeaderComponent),
                MockComponent(ServerListComponent)
            ]
        })
            .createComponent(ServerComponent);
        component = fixture.componentInstance;
        de = fixture.debugElement;
        fixture.detectChanges();
    });

    it('greets the header', () => {
        const container = de.query(By.css('app-header'));
        expect(container).toBeTruthy();
    });

    it('greets the main', () => {
        const container = de.query(By.css('app-header'));
        expect(container).toBeTruthy();
    });

});
