const RAM =  [
    {value: '2', display: '2GB', checked: false},
    {value: '4', display: '4GB', checked: false},
    {value: '8', display: '8GB', checked: false},
    {value: '12', display: '12GB', checked: false},
    {value: '16', display: '16GB', checked: false},
    {value: '24', display: '24GB', checked: false},
    {value: '32', display: '32GB', checked: false},
    {value: '48', display: '48GB', checked: false},
    {value: '64', display: '64GB', checked: false},
    {value: '96', display: '96GB', checked: false},
];

const HDD_STORAGE = {
    min: 0,
    max: 72000
};

export {
    RAM,
    HDD_STORAGE
};
