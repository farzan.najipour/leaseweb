import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from '../../../core/services';
import {SharedService} from '../../../services/shared.service';
import {Subscription} from 'rxjs';
import {NotifierService} from 'angular-notifier';
import {Server} from '../../../models/server.model';
import {style, state, animate, transition, trigger} from '@angular/animations';


@Component({
    selector: 'app-server-list',
    animations: [
        trigger('fadeInOut', [
            transition(':enter', [   // :enter is alias to 'void => *'
                style({opacity: 0}),
                animate(50, style({opacity: 1}))
            ]),
            transition(':leave', [   // :leave is alias to '* => void'
                animate(50, style({opacity: 0}))
            ])
        ])
    ],
    templateUrl: './server-list.component.html',
    styleUrls: ['./server-list.component.scss']
})
export class ServerListComponent implements OnInit, OnDestroy {
    servers: any[] = [];
    initialServers: any[] = [];
    filteredServers: any[] = [];
    isLoading = true;
    subscription: Subscription;
    private readonly notifier: NotifierService;

    constructor(private apiService: ApiService, private sharedService: SharedService, notifierService: NotifierService) {
        this.subscription = this.sharedService.getMessage().subscribe(message => {
            if (message.filter) {
                this.filterServerList(message.filter);
            }
        });

        this.notifier = notifierService;
    }

    ngOnInit(): void {
        this.loadServers();
    }

    /**
     * For checking new filter which comes from service and setting new value for filteredServers
     * @param filters
     */
    filterServerList(filters: any): void {
        // Reset server List
        this.filteredServers = this.initialServers;
        const filterByLocation = server => {
            if ('location' in filters) {
                return server.location === filters.location;
            }
            return true;
        };
        const filterByType = server => {
            if ('type' in filters) {
                return server.hdd.type === filters.type;
            }
            return true;
        };
        const filterByStorage = server => {
            if ('storage' in filters) {
                let memoryBase = server.hdd.memory;
                if (server.hdd.unit === 'TB') {
                    memoryBase *= 1000;
                }
                return (filters.storage.min <= memoryBase * server.hdd.count) &&
                    (filters.storage.max >= memoryBase * server.hdd.count);
            }
            return true;
        };
        const filterByRam = server => {
            if ('ram' in filters && filters.ram.length > 0) {
                return filters.ram.includes(server.ram.memory);
            }
            return true;
        };
        const filterByLocationResult = this.servers.filter(filterByLocation);
        const filterByTypeResult = this.servers.filter(filterByType);
        const filterByStorageResult = this.servers.filter(filterByStorage);
        const filterByRamResult = this.servers.filter(filterByRam);
        const locationIntersect = filterByLocationResult.filter(value => -1 !== filterByTypeResult.indexOf(value));
        const storageIntersect = locationIntersect.filter(value => -1 !== filterByStorageResult.indexOf(value));
        this.filteredServers = storageIntersect.filter(value => -1 !== filterByRamResult.indexOf(value));
    }

    /**
     * Load server from an API and using Server Interface
     */
    loadServers(): void {
        this.apiService.get('api/servers')
            .subscribe(
                response => {
                    this.servers = this.initialServers = response.servers as Server[];
                    this.filteredServers = this.filteredServers.length > 0 ? this.filteredServers : this.servers;
                    this.setLocations();
                    this.setHddTypes();
                    this.isLoading = false;
                },
                err => this.notifier.notify('warning', 'Network problem!')
            );
    }

    /**
     * Whenever is an API is calling, new cities is pushing to sharedService
     */
    setLocations(): void {
        let cities = [];
        this.servers.map(server => cities.push(server.location));
        cities = Array.from(new Set(cities));
        this.sharedService.setCities(cities);
    }

    /**
     * Whenever is an API is calling, new HDD Types is pushing to sharedService
     */
    setHddTypes(): void {
        let hddTypes = [];
        this.servers.map(server => hddTypes.push(server.hdd.type));
        hddTypes = Array.from(new Set(hddTypes));
        this.sharedService.setHddTypes(hddTypes);
    }

    /**

     * Unsubscribe to ensure no memory leaks
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
