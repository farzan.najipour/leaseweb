import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ServerComponent} from './server.component';
import {SearchComponent} from './search/search.component';
import {ServerListComponent} from './server-list/server-list.component';
import {ServerRoutes} from './server-routing.module';
import {Ng5SliderModule} from 'ng5-slider';
import {HeaderComponent} from './header/header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, ServerRoutes, Ng5SliderModule, BrowserAnimationsModule],
    declarations: [ServerComponent, SearchComponent, ServerListComponent, HeaderComponent ],
    providers: []
})
export class ServerModule {
}
