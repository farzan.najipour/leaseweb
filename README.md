# Leaseweb

This project which is related to Leaseweb's coding assignment was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.16.
You can see the result in : [https://gitlab.com/farzan.najipour/leaseweb](https://gitlab.com/farzan.najipour/leaseweb)


## Installation

1.  Make sure that you have Node.js v10.16.1 and npm v5 or above installed.
2.  Clone this repo 
3.  Move to the appropriate directory: `cd <YOUR_PROJECT_NAME>`.<br />
4. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).



## Screenshot

![alt text](src/assets/screenshot.png "Screenshot")


