import { browser, by, element } from 'protractor';

export class ServerPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  getAppHeaderElement() {
    return element(by.tagName('app-header'));
  }

  getAppHeader() {
    return element(by.css('app-header header'));
  }

  getAppHeaderImg() {
    return element(by.css('app-header img[src*=\'assets/leaseweb.svg\']'));
  }

  getAppSearchElement() {
    return element(by.tagName('app-search'));
  }

  getAppSearchHeaderText() {
    return element(by.css('app-search h3'));
  }

  getAppServerListElement() {
    return element(by.tagName('app-server-list'));
  }

  getAppServerListHeaderText() {
    return element(by.css('app-server-list h2'));
  }

  getSearchForm() {
    return element(by.css('app-search form'));
  }

}
