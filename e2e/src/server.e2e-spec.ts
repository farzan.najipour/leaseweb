import {browser, logging} from 'protractor';
import {ServerPage} from './server.po';

describe('Leaseweb e2e tests', () => {
    let page: ServerPage;
    beforeEach(() => {
        page = new ServerPage();
        page.navigateTo();
    });

    it('should display app header', () => {
        expect(page.getAppHeaderElement()).toBeTruthy();
        expect(page.getAppHeader()).toBeTruthy();
    });


    it('should display search box', () => {
        expect(page.getAppSearchElement()).toBeTruthy();
    });


    it('should display server list', () => {
        expect(page.getAppServerListElement()).toBeTruthy();
    });

    it('should count form in search box', () => {
        expect(page.getSearchForm()).toBeTruthy();
    });




    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });
});
